import sys
import getopt
from elog_parser.logbook_collector import collect_logbooks
import logging

logger = logging.getLogger("elog_import")

USAGE = "elog_import.py -p <logbooks root path>"


def main(argv):
    root_path = ''
    try:
        opts, args = getopt.getopt(argv, "hp:", ["path="])
    except getopt.GetoptError:
        print(USAGE)
        sys.exit(2)
    if len(opts) == 0:
        print(USAGE)
        sys.exit()
    for opt, arg in opts:
        if opt in ("-p", "--path"):
            root_path = arg
        else:
            print(USAGE)
            sys.exit()

    logbooks = collect_logbooks(root_path)
    for logbook in logbooks:
        logger.info("%s entry count: %d", logbook, len(logbooks[logbook]))


if __name__ == "__main__":
    main(sys.argv[1:])
