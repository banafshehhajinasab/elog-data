import logging
import os
try:
    import bbcode
except ImportError:
    logging.warning("No bbcode module installed; won't try to convert ELCode entries!")
    bbcode = None
from dateutil.parser import parse
logging.basicConfig(filename='/var/log/elog_parser.log', level=logging.DEBUG,
                    format='%(asctime)s %(levelname)s %(name)s %(message)s')
logger = logging.getLogger(__name__)

MID = "$@MID@$"
ATTACHMENT = "Attachment"
DATE = "Date"
WEEKDAY_EMPTY ="XXX,"


def process_elog_file(log_file_path, log_dir_path):
    logger.info("Parsing logfile '%s'", log_file_path)

    with open(log_file_path, encoding="ISO-8859-1") as f:
        content = f.read()

    entries = []
    for entry_text in content.split(MID + ":"):
        if not entry_text:
            continue
        entry = {}
        try:
            header, body = entry_text.split("=" * 40, 1)
        except ValueError as e:
            logger.error("Malformed entry!? %s: %r", log_file_path, e)
            continue
        header_lines = header.split("\n")
        mid = int(header_lines[0].strip())
        for line in header_lines[1:]:
            if line:
                try:
                    key, value = line.split(":", 1)
                    if key == ATTACHMENT:
                        attachments = []
                        if value.strip():
                            for attachment_file in value.strip().split(","):
                                attachments.append(os.path.join(log_dir_path, attachment_file))
                        entry[key] = attachments
                    elif key == DATE:
                        date_str = value.strip()
                        if date_str.startswith(WEEKDAY_EMPTY):
                            date_str = date_str.replace(WEEKDAY_EMPTY, "").strip()
                        entry[key] = parse(date_str)
                    else:
                        entry[key] = value.strip()
                except ValueError as ve:
                    logger.error("Line [%s] parse failed: %r", line, ve)
                    continue
        entry["mid"] = mid
        encoded_body = body.encode("utf-8")
        if encoded_body:
            entry["body"], entry["body_content_type"] = process_body(encoded_body, entry["Encoding"])
        entries.append(entry)

    return entries


def process_body(body, encoding):
    if encoding.lower() == "html":
        return body.decode("utf-8"), "text/html; charset=utf-8"
    if encoding.lower() == "plain":
        return body.decode("utf-8"), "text/plain; charset=utf-8"
    if encoding.lower() == "elcode":
        if bbcode:
            return (bbcode.render_html(body.decode("utf-8")),
                    "text/html; charset=utf-8")
        else:
            return body.decode("utf-8"), "text/plain; charset=utf-8"
