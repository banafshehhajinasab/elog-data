from elog_parser.log_file_parser import process_elog_file
import os


def collect_logbook_entries(logbook_path):
    entries = []
    year_dirs = (p for p in os.listdir(logbook_path) if os.path.isdir(os.path.join(logbook_path, p)))
    for year in year_dirs:
        year_dir_path = os.path.join(logbook_path, year)
        for file in os.listdir(year_dir_path):
            if file.endswith("a.log"):
                log_file_path = os.path.join(year_dir_path, file)
                entries.extend(process_elog_file(log_file_path, year_dir_path))
    return entries


def collect_logbooks(root_path):
    logbooks = {}
    logbook_dirs = (p for p in os.listdir(root_path) if os.path.isdir(os.path.join(root_path, p)))
    for logbook_dir in logbook_dirs:
        logbooks[logbook_dir] = collect_logbook_entries(os.path.join(root_path, logbook_dir))

    return logbooks
